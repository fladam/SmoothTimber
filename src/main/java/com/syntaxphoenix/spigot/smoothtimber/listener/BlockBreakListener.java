package com.syntaxphoenix.spigot.smoothtimber.listener;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import com.syntaxphoenix.spigot.smoothtimber.SmoothTimber;
import com.syntaxphoenix.spigot.smoothtimber.config.config.CutterConfig;
import com.syntaxphoenix.spigot.smoothtimber.event.AsyncPlayerTreeFallEvent;
import com.syntaxphoenix.spigot.smoothtimber.utilities.PlayerState;
import com.syntaxphoenix.spigot.smoothtimber.utilities.PluginUtils;
import com.syntaxphoenix.spigot.smoothtimber.utilities.limit.IntCounter;
import com.syntaxphoenix.spigot.smoothtimber.utilities.limit.Limiter;
import com.syntaxphoenix.spigot.smoothtimber.utilities.locate.Locator;
import com.syntaxphoenix.spigot.smoothtimber.version.manager.VersionChanger;
import com.syntaxphoenix.spigot.smoothtimber.version.manager.WoodType;
import com.syntaxphoenix.syntaxapi.random.NumberGeneratorType;
import com.syntaxphoenix.syntaxapi.random.RandomNumberGenerator;

public class BlockBreakListener implements Listener {

	private final RandomNumberGenerator generator = NumberGeneratorType.MURMUR.create(System.currentTimeMillis() >> 3);

	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onBlockBreak(BlockBreakEvent event) {
		if (event.isCancelled()) {
			return;
		}
		VersionChanger change = PluginUtils.CHANGER;
		if (!change.isWoodBlock(event.getBlock())) {
			return;
		}

		Player player = event.getPlayer();

		if (!PlayerState.isPermitted(player)) {
			return;
		}

		if (CutterConfig.ENABLE_WORLD) {
			boolean contains = CutterConfig.WORLD_LIST.contains(event.getBlock().getWorld().getName());
			if (CutterConfig.ENABLE_WORLD_BLACKLIST ? contains : !contains) {
				return;
			}
		}

		if (CutterConfig.SNEAK.test(() -> player.isSneaking())) {
			return;
		}

		if (CutterConfig.TOGGLEABLE.test(() -> SmoothTimber.STORAGE.hasToggled(player.getUniqueId()))) {
			return;
		}

		if (change.hasCuttingItemInHand(player)) {
			ItemStack tool = change.getItemInHand(player);
			if (!change.hasPermissionForCuttingItem(player, tool)) {
				return;
			}
			Location location = event.getBlock().getLocation();
			if (Locator.isPlayerPlaced(location)) {
				return;
			}
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskAsynchronously(PluginUtils.MAIN, new Runnable() {
				@Override
				public void run() {
					int maxItems = CutterConfig.ENABLE_LUCK ? change.getMaxDropCount(tool) : 1;
					ArrayList<Location> woodBlocks = new ArrayList<>();
					Location blockLocation = location;
					IntCounter counter = new IntCounter();
					int limit = Limiter.getLimit(player);
					int roots = CutterConfig.ROOT_DEPTH;
					int current = 0;
					for (int y = 0; y < 256; y++) {
						Locator.locateWood(new Location(blockLocation.getWorld(), blockLocation.getBlockX(),
							blockLocation.getBlockY() - CutterConfig.ROOT_DEPTH + y, blockLocation.getBlockZ()), woodBlocks, counter, limit);
						int value = counter.get();
						if (roots-- <= 0 && (value == limit || value == current)) {
							break;
						}
						current = value;
					}
					if (SmoothTimber.triggerChopEvent(player, location, change, tool, woodBlocks, limit)) {
						return;
					}
					Bukkit.getScheduler().runTask(PluginUtils.MAIN, new Runnable() {
						@Override
						public void run() {
							AsyncPlayerTreeFallEvent event = SmoothTimber.buildFallEvent(player, location, change, tool);
							for (int index = 0; index < woodBlocks.size(); index++) {
								Block block = woodBlocks.get(index).getBlock();
								WoodType wood = change.getWoodTypeFromBlock(block);
								if (change.hasPermissionForWoodType(player, wood)) {
									if (change.removeDurabilityFromItem(tool) == null) {
										break;
									}
									event.add(wood);
									change.toFallingBlock(block).setMetadata("STAnimate",
										new FixedMetadataValue(SmoothTimber.get(), maxItems <= 1 ? maxItems : generateAmount(maxItems)));
								}
							}
							Bukkit.getScheduler().runTaskAsynchronously(PluginUtils.MAIN, () -> SmoothTimber.triggerFallEvent(event));
						}

						private int generateAmount(int max) {
							int drop = 1;
							float more = 1f / (max + 1);
							float previous = more * 2f;
							float next = more * 3f;
							float chance = generator.nextFloat() * (float) CutterConfig.LUCK_MULTIPLIER;
							while (true) {
								if (previous < chance && chance > next) {
									drop++;
									previous = next;
									next += more;
								} else if (previous < chance && chance < next) {
									drop++;
									break;
								} else {
									break;
								}
							}
							if (drop > 64)
								return 64;
							return drop;
						}

					});
				}
			});
		}

	}
}
